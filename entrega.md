# ENTREGA CONVOCATORIA ENERO
- Maiara García Morán
- Correo: m.garciamo.2022@alumnos.urjc.es
- Enlace al vídeo: https://youtu.be/UKmYmlBwQHk
- Requisitos mínimos:
  - change_colors
  - rotate_right
  - mirror
  - rotate_colors
  - blur
  - shift
  - crop
  - grayscale
  - filter
  - transform_simple.py
  - transform_args.py
  - transform_multi.py
- Requisitos opcionales:
  - Filtros avanzados: sepia
