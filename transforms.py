def change_colors(image: list[list[tuple[int, int, int]]], to_change: tuple[int, int, int], to_change_to: tuple[int, int, int]) -> list[list[tuple[int, int, int]]]:
    rows = len(image)
    cols = len(image[0])

    for i in range(rows):
        for j in range(cols):
            pixel = image[i][j]
            if pixel == to_change:
                image[i][j] = to_change_to

    return image

def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    rows = len(image)
    cols = len(image[0])

    rotated_image = []

    for i in range(cols):
        rotated_image.append([])
        for j in range(rows):
            rotated_image[i].append(image[j][-i])
    return rotated_image

def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:

    image.reverse()

    return image

def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
    rows = len(image)
    cols = len(image[0])

    rotated_colors_image = [[(0, 0, 0) for _ in range(cols)] for _ in range(rows)]

    for i in range(rows):
        for j in range(cols):
            pixel = image[i][j]
            rotated_pixel = tuple((channel + increment) % 256 for channel in pixel)
            rotated_colors_image[i][j] = rotated_pixel

    return rotated_colors_image

def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    rows = len(image)
    cols = len(image[0])

    blurred_image = [[(0, 0, 0) for _ in range(cols)] for _ in range(rows)]

    for i in range(rows):
        for j in range(cols):
            total_pixels = 0
            total_r = 0
            total_g = 0
            total_b = 0

            for ni, nj in ((i-1, j), (i+1, j), (i, j-1), (i, j+1)):
                if 0 <= ni < rows and 0 <= nj < cols:
                    total_pixels += 1
                    pixel = image[ni][nj]
                    total_r += pixel[0]
                    total_g += pixel[1]
                    total_b += pixel[2]

            if total_pixels > 0:
                blurred_image[i][j] = (total_r // total_pixels, total_g // total_pixels, total_b // total_pixels)
            else:
                blurred_image[i][j] = image[i][j]

    return blurred_image

def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:
    rows = len(image)
    cols = len(image[0])

    shifted_image = [[(0, 0, 0) for _ in range(cols)] for _ in range(rows)]

    for i in range(rows):
        for j in range(cols):
            new_i = i + vertical
            new_j = j + horizontal

            if 0 <= new_i < rows and 0 <= new_j < cols:
                shifted_image[new_i][new_j] = image[i][j]

    return shifted_image

def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int) -> list[list[tuple[int, int, int]]]:
    rows = len(image)
    cols = len(image[0])

    cropped_image = [[(0, 0, 0) for _ in range(width)] for _ in range(height)]
    if x < 0 or y < 0 or x + width > cols or y + height > rows:
        raise ValueError("Rectángulo especificado está fuera de los límites de la imagen")

    for i in range(height):
        for j in range(width):
            cropped_image[i][j] = image[y + i][x + j]

    return cropped_image

def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    rows = len(image)
    cols = len(image[0])

    grayscale_image = [[(0, 0, 0) for _ in range(cols)] for _ in range(rows)]

    for i in range(rows):
        for j in range(cols):
            avg_value = sum(image[i][j]) // 3
            grayscale_image[i][j] = (avg_value, avg_value, avg_value)

    return grayscale_image

def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:
    rows = len(image)
    cols = len(image[0])

    filtered_image = [[(0, 0, 0) for _ in range(cols)] for _ in range(rows)]

    for i in range(rows):
        for j in range(cols):
            new_r = min(int(image[i][j][0] * r), 255)
            new_g = min(int(image[i][j][1] * g), 255)
            new_b = min(int(image[i][j][2] * b), 255)

            filtered_image[i][j] = (new_r, new_g, new_b)

    return filtered_image

def sepia(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:
    rows = len(image)
    cols = len(image[0])

    for i in range(rows):
        for j in range(cols):
            old_r, old_g, old_b = image[i][j]

            new_r = int((old_r * r) + (old_g * g) + (old_b * b))
            new_g = int((old_r * 0.349) + (old_g * 0.686) + (old_b * 0.168))
            new_b = int((old_r * 0.272) + (old_g * 0.534) + (old_b * 0.131))

            new_r = max(0, min(new_r, 255))
            new_g = max(0, min(new_g, 255))
            new_b = max(0, min(new_b, 255))

            image[i][j] = (new_r, new_g, new_b)

    return image






